﻿using Redmine.Commons;
using SeleniumSharp;

namespace Redmine.Application {

    public class Browser {
        private static Chrome _chrome;
        private static object Locker = new object( );

        public static void Close( ) {
            if ( _chrome != null && _chrome.IsRunning )
                _chrome.Dispose( );
        }

        public static Chrome GetInstance( ) {
            lock ( Locker ) {
                if ( _chrome == null || !_chrome.IsRunning ) {
                    _chrome = new Chrome( Settings.RedmineUrl, Settings.Visual, Settings.AppData, "--no-sandbox" );
                }
                return _chrome;
            }
        }
    }
}