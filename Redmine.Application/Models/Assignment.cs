﻿using LiteDB;
using Redmine.Application.Services;
using System;
using System.Collections.Generic;

namespace Redmine.Application.Models {

    public class Assignment {
        public long AssignmentId { get; set; }
        public string Description { get; set; }
        public List<string> Modules { get; set; } = new List<string>( );
        public int Percent { get; set; }
        public string Priority { get; set; }

        [BsonRef]
        public Project Project { get; set; }

        public string Status { get; set; }
        public string TextForVersion { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public string Version { get; set; }
        public long ParentId { get; set; }
        public DateTime? UpdatedAt { get; set; }

        [BsonIgnore]
        public IEnumerable<Assignment> Child => new AssignmentService( ).GetAll( x => x.ParentId == AssignmentId );

        [BsonIgnore]
        public Assignment Parent => new AssignmentService( ).Get( x => x.AssignmentId == ParentId );

        public Assignment( string title, string description, string type, string priority, string status, string version, string textForVersion, long parentId, List<string> modules, Project project ) {
            Title = title;
            Description = description;
            Type = type;
            Priority = priority;
            Status = status;
            Version = version;
            TextForVersion = textForVersion;
            ParentId = parentId;
            Modules = modules;
            Project = project ?? throw new Exception( "Projeto não existe!" );
            UpdatedAt = DateTime.Now;
        }

        public Assignment( ) {
            UpdatedAt = DateTime.Now;
        }

        public void UpdatePercent( int value ) {
            if ( value >= 0 && value <= 100 && value % 10 == 0 )
                Percent = value;
            throw new Exception( "O valor deve ser MAIOR que zero, MENOR que 100" );
        }
    }
}