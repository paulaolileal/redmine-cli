﻿using LiteDB;
using System;

namespace Redmine.Application.Models {

    public class Project {
        public string Description { get; set; }
        public string Name { get; set; }

        [BsonRef]
        public ProjectValue AllowedValues { get; set; }

        [BsonId( false )]
        public string ProjectId { get; set; }

        public string ParentId { get; set; }

        [BsonField]
        public DateTime? UpdatedAt { get; set; }

        [BsonField]
        public DateTime? AssignmentUpdatedAt { get; set; }

        [BsonField]
        public DateTime? TimeEntryUpdatedAt { get; set; }

        [BsonIgnore]
        public bool IsRoot => string.IsNullOrEmpty( ParentId );

        public void GenerateId( ) {
            ProjectId = Name
                .Trim( )
                .Replace( " ", "-" )
                .ToLower( );
        }

        public void Update( ) {
            UpdatedAt = DateTime.Now;
        }

        public void UpdateAssignment( ) {
            AssignmentUpdatedAt = DateTime.Now;
        }

        public void UpdateTimeEntries( ) {
            TimeEntryUpdatedAt = DateTime.Now;
        }
    }
}