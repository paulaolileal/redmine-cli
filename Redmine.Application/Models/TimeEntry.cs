﻿using LiteDB;
using System;

namespace Redmine.Application.Models {

    public class TimeEntry {
        public string Activity { get; set; }

        [BsonRef]
        public Assignment Assignment { get; set; }

        public string Comments { get; set; }

        public DateTime? DateEnd { get; set; }

        public DateTime DateStart { get; set; }

        public long RedmineId { get; set; }

        public bool Submited { get; set; }

        [BsonIgnore]
        public TimeSpan Time {
            get {
                var date = DateEnd ?? DateTime.Now;
                return date - DateStart;
            }
        }

        [BsonId]
        public long TimeEntryId { get; set; }

        public TimeEntry( ) {
        }

        public TimeEntry( Assignment assignment, string activity, string comments, DateTime start ) {
            Comments = comments;
            Assignment = assignment;
            Activity = activity;
            DateStart = start;
        }

        public TimeEntry( Assignment assignment, string activity, string comments, DateTime start, DateTime end ) {
            Comments = comments;
            Assignment = assignment;
            Activity = activity;
            DateStart = start;
            DateEnd = end;
        }

        public TimeEntry( Assignment assignment, string activity, string comments, long redmineId, DateTime start, TimeSpan time ) {
            Comments = comments;
            RedmineId = redmineId;
            Assignment = assignment;
            Activity = activity;
            DateStart = start;
            DateEnd = start + time;
            Submited = true;
        }

        public void Finalizar( DateTime dateTime ) {
            DateEnd = dateTime;
        }
    }
}