﻿using LiteDB;

namespace Redmine.Application.Models {

    public class User {
        public bool Global { get; set; }

        [BsonRef]
        public Member Member { get; set; }

        public string Password { get; set; }
        public string RedmineUrl { get; set; }
        public long UserId { get; set; }
        public string Username { get; set; }

        public User( string username, string password, string redmineUrl, bool global = false ) {
            Username = username;
            Password = password;
            RedmineUrl = redmineUrl;
            Global = global;
        }
    }
}