﻿using LiteDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Redmine.Application.Services.Base {

    public abstract class Service<T>: IDisposable {
        protected readonly ILiteCollection<T> _collection;
        protected readonly LiteDatabase _db;

        public Service( ) {
            _db = Database.DB;
            _collection = _db.GetCollection<T>( );
        }

        public virtual BsonValue Add( T entity ) {
            return _collection.Insert( entity );
        }

        public virtual T AddOrUpdate( T entity ) {
            _collection.Upsert( entity );
            return entity;
        }

        public virtual int AddRange( IEnumerable<T> entities ) {
            return _collection.Insert( entities );
        }

        public virtual int Delete( Expression<Func<T, bool>> predicate ) {
            return _collection.DeleteMany( predicate );
        }

        public virtual int DeleteAll( ) {
            return _collection.DeleteAll( );
        }

        public virtual void Dispose( ) => _db.Dispose( );

        public virtual bool Exists( T entity ) {
            ( BsonMapper.Global.ToDocument( entity ) ).TryGetValue( "_id", out var id );
            return _collection.Exists( BsonExpression.Create( $"_id = {id}" ) );
        }

        public virtual T Get( Expression<Func<T, bool>> predicate ) {
            return _collection.FindOne( predicate );
        }

        public virtual IEnumerable<T> GetAll( ) {
            return _collection.FindAll( ).ToList( );
        }

        public virtual IEnumerable<T> GetAll( Expression<Func<T, bool>> predicate ) {
            return _collection.Find( predicate );
        }

        public virtual BsonValue Update( T entity ) {
            return _collection.Update( entity );
        }

        public int Update( IEnumerable<T> entities ) {
            return _collection.Update( entities );
        }
    }
}