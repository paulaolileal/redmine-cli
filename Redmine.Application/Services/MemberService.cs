﻿using Myth.Extensions;
using OpenQA.Selenium;
using Redmine.Application.Models;
using Redmine.Application.Services.Base;
using Redmine.Commons;
using ShellProgressBar;
using System.Collections.Generic;
using System.Linq;

namespace Redmine.Application.Services {

    public class MemberService: Service<Member> {

        public Member GetMemberForCurrentUser( ) {
            var chrome = Browser.GetInstance( );
            chrome
                .NavigateTo( $"my/account" );

            var firstName = chrome.Search( "//*[@id=\"user_firstname\"]" ).GetAttribute( "value" );

            var lastName = chrome.Search( "//*[@id=\"user_lastname\"]" ).GetAttribute( "value" );

            var memberName = firstName + " " + lastName;

            var member = base.Get( x => x.Name == memberName );

            if ( member == null ) {
                var projects = chrome.SearchAll( "//*[@id=\"content\"]/div[2]/ul[2]/li" );
                if ( projects.Any( ) ) {
                    var role = projects.First( ).Text.GetStringBetween( '(', ',' );
                    member = new Member( memberName, role );
                    base.Add( member );
                }
            }

            return member;
        }

        public void Sync( IEnumerable<Project> projects ) {
            var chrome = Browser.GetInstance( );

            using var loader = new ProgressBar( projects.Count( ), "Sincronizando colaboradores...", Settings.Options );

            foreach ( var project in projects ) {
                chrome
                    .NavigateTo( $"projects/{project.ProjectId}" );

                var roles = chrome.SearchAll( "//div[contains(@class, \"members\")]/p" );

                var membersList = new List<Member>( );

                foreach ( var role in roles ) {
                    var roleName = role.FindElement( By.TagName( "span" ) ).Text.Replace( ":", "" );
                    var members = role.FindElements( By.TagName( "a" ) ).Select( x => x.Text );
                    membersList.AddRange( members.Select( x => new Member( x, roleName ) ) );
                }

                foreach ( var item in membersList ) {
                    if ( Get( x => x.Name == item.Name ) == null )
                        base.Add( item );
                }

                loader.Tick( $"Atualizado colaboradores do projeto {project.Name}" );
            }
        }
    }
}