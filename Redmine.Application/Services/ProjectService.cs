﻿using LiteDB;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Redmine.Application.Models;
using Redmine.Application.Services.Base;
using Redmine.Commons;
using Redmine.Commons.Extensions;
using ShellProgressBar;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Redmine.Application.Services {

    public class ProjectService: Service<Project> {

        public override BsonValue Add( Project project ) {
            var chrome = Browser.GetInstance( );
            chrome
                .NavigateTo( $"projects/new" );

            chrome.Search( "//*[@id=\"project_name\"]" ).SendKeys( project.Name );

            chrome.Search( "//*[@id=\"project_description\"]" ).SendKeys( project.Description );

            var projectValueService = new ProjectValueService( );

            var idField = chrome.Search( "//*[@id=\"project_identifier\"]" );
            idField.Clear( );
            idField.SendKeys( project.ProjectId );

            chrome.Search( "//*[@id=\"new_project\"]/input[3]" ).Click( );

            chrome.NavigateTo( $"projects/{project.ProjectId}/issues" );

            new SelectElement( chrome.Search( "//*[@id=\"add_filter_select\"]" ) ).SelectByText( "Tipo" );
            chrome.Wait( 200 );
            var optionsType = new SelectElement( chrome.Search( "//*[@id=\"values_tracker_id_1\"]" ) ).Options;
            var allowedAssignmentTypes = optionsType
                .OrderByDescending( x => x.GetAttribute( "selected" ) )
                .Select( x => x.Text )
                .ToList( );
            projectValueService.AddOrUpdateAssignmentTypes( project.ProjectId, allowedAssignmentTypes );

            new SelectElement( chrome.Search( "//*[@id=\"operators_status_id\"]" ) ).SelectByValue( "=" );
            var optionsStatus = new SelectElement( chrome.Search( "//*[@id=\"values_status_id_1\"]" ) ).Options;
            var allowedStatus = optionsStatus
                .OrderByDescending( x => x.GetAttribute( "selected" ) )
                .Select( x => x.Text )
                .ToList( );
            projectValueService.AddOrUpdateStatus( project.ProjectId, allowedStatus );

            new SelectElement( chrome.Search( "//*[@id=\"add_filter_select\"]" ) ).SelectByText( "Prioridade" );
            chrome.Wait( 200 );
            var optionsPriority = new SelectElement( chrome.Search( "//*[@id=\"values_priority_id_1\"]" ) ).Options;
            var allowedPriority = optionsPriority
                .OrderByDescending( x => x.GetAttribute( "selected" ) )
                .Select( x => x.Text )
                .ToList( );
            projectValueService.AddOrUpdatePriority( project.ProjectId, allowedStatus );

            project.AllowedValues = projectValueService.LoadOrCreate( project.ProjectId );

            return base.Add( project );
        }

        public void Sync( bool allProjects = false ) {
            var projectValueService = new ProjectValueService( );

            var chrome = Browser.GetInstance( );

            chrome.NavigateTo( "projects" );

            var projectsLinks = chrome.SearchAll( $"//a[(contains(@class,'project')) and {( !allProjects ? "(contains(@class,'my-project')) and" : "" )} not(contains(@class, 'projects'))]/.." ).ToList( );

            var projects = new List<Project>( );

            using var loader = new ProgressBar( 2, "Sincronizando projetos...", Settings.Options );

            using var projectLoader = loader.Spawn( projectsLinks.Count, "Carregando projetos", Settings.ChildOptions );

            foreach ( (var item, int index) in projectsLinks.WithIndex( ) ) {
                var project = new Project( );

                var a = item.FindElement( By.TagName( "a" ) );
                project.ProjectId = a.GetAttribute( "href" ).Split( '/' ).Last( );
                project.Name = a.Text;

                var description = item.FindElements( By.ClassName( "wiki" ) );
                project.Description = description.FirstOrDefault( )?.Text;

                var root = item.FindElements( By.ClassName( "root" ) );
                if ( !root.Any( ) )
                    project.ParentId = item.FindElement( By.XPath( "../../.." ) ).FindElement( By.TagName( "a" ) ).GetAttribute( "href" ).Split( '/' ).Last( );

                project.AllowedValues = projectValueService.LoadOrCreate( project.ProjectId );

                projects.Add( project );

                projectLoader.Tick( $"Atualizado projeto: {project.Name}" );
            }

            loader.Tick( );

            using var valuesLoader = loader.Spawn( projectsLinks.Count, "Carregando informações", Settings.ChildOptions );

            foreach ( (var project, var index) in projects.WithIndex( ) ) {
                if ( project.AllowedValues != null &&
                    ( !project.AllowedValues.AssignmentTypes.Any( ) ||
                    !project.AllowedValues.Priority.Any( ) ||
                    !project.AllowedValues.Status.Any( ) ) &&
                    ( !project.AllowedValues.UpdatedAt.HasValue || DateTime.Now - project.AllowedValues.UpdatedAt > TimeSpan.FromDays( 7 ) ) ) {
                    chrome.NavigateTo( $"projects/{project.ProjectId}/issues" );

                    var filters = new SelectElement( chrome.Search( "//*[@id=\"add_filter_select\"]" ) );

                    filters.SelectByText( "Tipo" );
                    var optionsType = new SelectElement( chrome.Search( "//*[@id=\"values_tracker_id_1\"]" ) ).Options;
                    project.AllowedValues = projectValueService.AddOrUpdateAssignmentTypes( project.ProjectId, optionsType.Select( x => x.Text ).ToList( ) );

                    new SelectElement( chrome.Search( "//*[@id=\"operators_status_id\"]" ) ).SelectByValue( "=" );
                    var optionsStatus = new SelectElement( chrome.Search( "//*[@id=\"values_status_id_1\"]" ) ).Options;
                    project.AllowedValues = projectValueService.AddOrUpdateStatus( project.ProjectId, optionsStatus.Select( x => x.Text ).ToList( ) );

                    filters.SelectByText( "Prioridade" );
                    var optionsPriority = new SelectElement( chrome.Search( "//*[@id=\"values_priority_id_1\"]" ) ).Options;
                    project.AllowedValues = projectValueService.AddOrUpdatePriority( project.ProjectId, optionsPriority.Select( x => x.Text ).ToList( ) );

                    if ( filters.Options.Any( x => x.Text == "Versão" ) ) {
                        filters.SelectByText( "Versão" );
                        var versions = new SelectElement( chrome.Search( "//*[@id=\"values_fixed_version_id_1\"]" ) ).Options;
                        project.AllowedValues = projectValueService.AddOrUpdateVersions( project.ProjectId, versions.Select( x => x.Text.Replace( project.Name + " - ", "" ) ).ToList( ) );
                    }

                    if ( filters.Options.Any( x => x.Text == "Módulo SGAP" ) ) {
                        filters.SelectByText( "Módulo SGAP" );
                        var modulesSgap = new SelectElement( chrome.Search( "//*[@id=\"values_cf_8_1\"]" ) ).Options;
                        project.AllowedValues = projectValueService.AddOrUpdateModules( project.ProjectId, modulesSgap.Select( x => x.Text ).ToList( ) );
                    }

                    project.AllowedValues.Update( );
                    project.Update( );

                    projectValueService.Update( project.AllowedValues );
                }

                var exists = Get( x => x.ProjectId == project.ProjectId );
                if ( exists == null )
                    base.Add( project );
                else {
                    project.AssignmentUpdatedAt = exists.AssignmentUpdatedAt;
                    Update( project );
                }

                valuesLoader.Tick( $"Atualizado informações de {project.Name}" );
            }

            loader.Tick( );
        }

        public override Project Get( Expression<Func<Project, bool>> predicate ) {
            return _collection
                .Include( x => x.AllowedValues )
                .FindOne( predicate );
        }

        public override IEnumerable<Project> GetAll( ) {
            return _collection
                .Include( x => x.AllowedValues )
                .FindAll( );
        }
    }
}