﻿using LiteDB;
using Myth.Extensions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using Redmine.Application.Models;
using Redmine.Application.Services.Base;
using Redmine.Commons;
using Redmine.Commons.Extensions;
using ShellProgressBar;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;

namespace Redmine.Application.Services {

    public class TimeEntryService: Service<TimeEntry> {

        public override int Delete( Expression<Func<TimeEntry, bool>> predicate ) {
            return base.Delete( predicate );
        }

        public override bool Exists( TimeEntry entity ) {
            return _collection.Exists( x => x.RedmineId == entity.RedmineId );
        }

        public override TimeEntry Get( Expression<Func<TimeEntry, bool>> predicate ) {
            return _collection
                .Include( x => x.Assignment )
                .Include( x => x.Assignment.Project )
                .Include( x => x.Assignment.Project.AllowedValues )
                .FindOne( predicate );
        }

        public override IEnumerable<TimeEntry> GetAll( Expression<Func<TimeEntry, bool>> predicate ) {
            return _collection
                .Include( x => x.Assignment )
                .Include( x => x.Assignment.Project )
                .Include( x => x.Assignment.Project.AllowedValues )
                .Find( predicate );
        }

        public override IEnumerable<TimeEntry> GetAll( ) {
            return _collection
                .Include( x => x.Assignment )
                .Include( x => x.Assignment.Project )
                .Include( x => x.Assignment.Project.AllowedValues )
                .FindAll( )
                .ToList( );
        }

        public string GetTimeByProject( long assignmentId ) {
            var times = GetAll( x => x.Assignment.AssignmentId == assignmentId );

            return TimeSpan.FromSeconds( times.Sum( x => x.Time.TotalSeconds ) ).ToString( "h\\:mm", CultureInfo.InvariantCulture );
        }

        public void Submit( IEnumerable<TimeEntry> entities ) {
            using var loader = new ProgressBar( entities.Count( ), "Enviando tempos de trabalho", Settings.Options );
            foreach ( (var item, var index) in entities.WithIndex( ) ) {
                Submit( item, loader );
                loader.Tick( $"Enviado {index} de {entities.Count( )}" );
            }
        }

        public BsonValue Submit( TimeEntry entity, IProgressBar loader ) {
            var chrome = Browser.GetInstance( );

            var timesBefore = GetUserTimes( entity.Assignment.Project, loader, entity.Assignment ); ;

            chrome.NavigateTo( $"issues/{entity.Assignment.AssignmentId}/time_entries/new" );

            var dateField = chrome.Search( "//*[@id=\"time_entry_spent_on\"]" );
            dateField.Clear( );

            dateField.SendKeys( entity.DateStart.ToString( "MM/dd/yyyy" ) );

            chrome.Search( "//*[@id=\"time_entry_hours\"]" ).SendKeys( entity.Time.ToString( "h\\:mm", CultureInfo.InvariantCulture ) );

            if ( !string.IsNullOrEmpty( entity.Comments ) )
                chrome.Search( "//*[@id=\"time_entry_comments\"]" ).SendKeys( entity.Comments );

            var activity = new SelectElement( chrome.Search( "//*[@id=\"time_entry_activity_id\"]" ) );
            if ( !string.IsNullOrEmpty( entity.Activity ) )
                activity.SelectByText( entity.Activity, true );

            chrome.Search( "//*[@id=\"new_time_entry\"]/input[4]" ).Click( );

            var timesAfter = GetUserTimes( entity.Assignment.Project, loader, entity.Assignment );

            var timeNew = timesAfter
                .Except( timesBefore )
                .Where( x => x.Time.ToString( "h\\:mm", CultureInfo.InvariantCulture ) == entity.Time.ToString( "h\\:mm", CultureInfo.InvariantCulture ) )
                .FirstOrDefault( );

            if ( timeNew != null )
                entity.RedmineId = timeNew.RedmineId;

            entity.Submited = true;

            if ( !entity.DateEnd.HasValue )
                entity.DateEnd = DateTime.Now;

            return base.Update( entity );
        }

        public void Sync( IEnumerable<Project> projects ) {
            using var loader = new ProgressBar( projects.Count( ), "Sincronizando tempos de trabalho por projetos...", Settings.Options );

            foreach ( var project in projects ) {
                Sync( project, loader );
                loader.Tick( $"Atualizado tempos de trabalho do projeto {project.Name}" );
                project.UpdateTimeEntries( );
                new ProjectService( ).Update( project );
            }
        }

        public void Sync( IEnumerable<Assignment> assignments ) {
            using var loader = new ProgressBar( assignments.Count( ), "Sincronizando tempos de trabalho por demandas...", Settings.Options );

            foreach ( var assignment in assignments ) {
                Sync( assignment.Project, loader, assignment );
                loader.Tick( $"Atualizado tempos de trabalho da demanda {assignment.Title}" );
            }
        }

        public List<TimeEntry> GetUserTimes( Project project, IProgressBar loader, Assignment assignment = null ) {
            var list = new List<TimeEntry>( );
            var chrome = Browser.GetInstance( );

            chrome.NavigateTo( $"projects/{project.ProjectId}/time_entries?utf8=✓&set_filter=1&sort=spent_on%3Adesc{( project.TimeEntryUpdatedAt.HasValue ? "&f%5B%5D=spent_on&op%5Bspent_on%5D=>%3D&v%5Bspent_on%5D%5B%5D=" + project.TimeEntryUpdatedAt.Value.ToString( "yyyy-MM-dd" ) : "" )}&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=me&f%5B%5D=&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours&group_by=&t%5B%5D=hours&t%5B%5D=&per_page=100{( assignment != null ? "&issue_id=" + assignment.AssignmentId : "" )}" );
            if ( !chrome.Driver.Title.Contains( "404" ) ) {
                var nextPage = chrome.SearchAll( "//li[contains(@class,\"next\")]/preceding::a[1]" );

                var totalPages = nextPage.Any( ) ? Convert.ToInt32( nextPage.First( ).Text ) : 1;

                using var pagesLoader = loader.Spawn( totalPages, $"Carregando tempos de trabalho, total de paginas {totalPages}", Settings.ChildOptions );
                for ( int page = 1; page <= totalPages; page++ ) {
                    list.AddRange( GetUserTimes( project, page, loader, assignment ) );
                    pagesLoader.Tick( $"Carregando tempos de trabalho, pagina {page} de {totalPages}" );
                }
            }

            return list.Distinct( ).ToList( );
        }

        private void Sync( Project project, IProgressBar loader, Assignment assignment = null ) {
            var rows = GetUserTimes( project, loader, assignment );

            var alreadyExists = rows.Where( x => Exists( x ) ).ToList( );
            var notExists = rows.Except( alreadyExists ).ToList( );

            base.AddRange( notExists );
            Update( alreadyExists );
        }

        private List<TimeEntry> GetUserTimes( Project project, int page, IProgressBar loader, Assignment assignment = null ) {
            var chrome = Browser.GetInstance( );
            var assignmentService = new AssignmentService( );
            chrome.NavigateTo( $"projects/{project.ProjectId}/time_entries?utf8=✓&set_filter=1&sort=spent_on%3Adesc{( project.TimeEntryUpdatedAt.HasValue ? "&f%5B%5D=spent_on&op%5Bspent_on%5D=>%3D&v%5Bspent_on%5D%5B%5D=" + project.TimeEntryUpdatedAt.Value.ToString( "yyyy-MM-dd" ) : "" )}&f%5B%5D=user_id&op%5Buser_id%5D=%3D&v%5Buser_id%5D%5B%5D=me&f%5B%5D=&c%5B%5D=spent_on&c%5B%5D=user&c%5B%5D=activity&c%5B%5D=issue&c%5B%5D=comments&c%5B%5D=hours&group_by=&t%5B%5D=hours&t%5B%5D=&per_page=100&page={page}{( assignment != null ? "&issue_id=" + assignment.AssignmentId : "" )}" );

            var rows = chrome.SearchAll( "//tr[contains(@class, \"time-entry\")]" ).ToList( );

            var list = new List<TimeEntry>( );

            using var assignmentLoader = loader.Spawn( rows.Count, "Carregando demandas", Settings.ChildOptions );

            foreach ( var item in rows ) {
                var day = item.FindElement( By.ClassName( "spent_on" ) ).Text;
                var activity = item.FindElement( By.ClassName( "activity" ) ).Text;
                var comments = item.FindElement( By.ClassName( "comments" ) ).Text;
                var redmineId = Convert.ToInt64( item.FindElement( By.Name( "ids[]" ) ).GetAttribute( "value" ) );

                var hours = item.FindElement( By.ClassName( "hours" ) ).Text;
                var time = TimeSpan.ParseExact( hours, "h\\:mm", CultureInfo.InvariantCulture );
                var start = DateTime.ParseExact( day, "dd/MM/yyyy", CultureInfo.InvariantCulture );

                var assign = assignment;

                if ( assign == null ) {
                    var issue = item.FindElement( By.ClassName( "issue" ) ).Text.GetStringBetween( '#', ':' );
                    assign = assignmentService.Get( x => x.AssignmentId == Convert.ToInt64( issue ) );
                }

                if ( assign != null ) {
                    var timeEntry = new TimeEntry( assign, activity, comments, redmineId, start, time );
                    list.Add( timeEntry );
                }

                assignmentLoader.Tick( $"Atualizando tempo de trabalho" );
            }
            return list;
        }
    }
}