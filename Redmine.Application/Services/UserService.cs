﻿using Redmine.Application.Models;
using Redmine.Application.Services.Base;
using Redmine.Commons;
using ShellProgressBar;
using System;
using System.Linq;

namespace Redmine.Application.Services {

    public class UserService: Service<User> {

        public User GetGlobal( ) {
            return _collection
                 .Include( x => x.Member )
                 .FindOne( x => x.Global );
        }

        public void Login( ) {
            var userGlobal = Get( x => x.Global );
            if ( userGlobal == null )
                throw new Exception( "Não existe usuário para logar!" );

            using ( var loader = new ProgressBar( 6, "Autenticação em andamento", Settings.Options ) ) {
                loader.Tick( );

                var chrome = Browser.GetInstance( );
                chrome
                    .NavigateTo( $"login" );

                loader.Tick( );

                var alreadyLogged = chrome.SearchAll( "//*[@id=\"loggedas\"]" );

                if ( !alreadyLogged.Any( ) ) {
                    chrome.Search( "//*[@id=\"username\"]" ).SendKeys( userGlobal.Username );

                    loader.Tick( );

                    chrome.Search( "//*[@id=\"password\"]" ).SendKeys( userGlobal.Password );

                    loader.Tick( );

                    chrome.Search( "//*[@id=\"login-submit\"]" ).Click( );

                    loader.Tick( );

                    var logoutButton = chrome.SearchAll( "//a[contains(@class, 'logout')]" );

                    if ( !logoutButton.Any( ) )
                        throw new Exception( "Erro ao fazer o login!" );

                    loader.Tick( "Autenticado com sucesso!" );
                }
            }
        }
    }
}