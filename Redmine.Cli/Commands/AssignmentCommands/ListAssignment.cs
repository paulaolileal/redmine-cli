﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Redmine.Cli.Commands.AssignmentCommands {

    [Command( "list", "l", Description = "Listar as demandas" )]
    public class ListAssignment: CommandBase {

        [Option( CommandOptionType.MultipleValue, Template = "-i|--issue", Description = "Id da demanda" )]
        public IEnumerable<long> AssignmentId { get; set; }

        [Option( CommandOptionType.MultipleValue, ShortName = "pr", Description = "Tipo de prioridade" )]
        public IEnumerable<string> Priority { get; set; }

        [Option( CommandOptionType.MultipleValue, Description = "Id do projeto" )]
        public IEnumerable<string> ProjectId { get; set; }

        [Option( CommandOptionType.MultipleValue, Description = "Tipo de demanda" )]
        public IEnumerable<string> Type { get; set; }

        [Option( CommandOptionType.MultipleValue, Description = "Status de demanda" )]
        public IEnumerable<string> Status { get; set; }

        [Option( CommandOptionType.MultipleValue, Template = "-es|--except-status", Description = "Exceto os status de demanda" )]
        public IEnumerable<string> ExceptStatus { get; set; }

        private readonly AssignmentService _assignmentService;
        private readonly TimeEntryService _timeEntryService;

        public ListAssignment( ) {
            _assignmentService = new AssignmentService( );
            _timeEntryService = new TimeEntryService( );
        }

        public override void OnExecute( ) {
            var assignments = _assignmentService
                .GetAll( x => x.ParentId == 0 )
                .OrderBy( x => x.AssignmentId )
                .AsEnumerable( );

            if ( AssignmentId?.Any( ) == true ) {
                assignments = _assignmentService
                    .GetAll( )
                    .OrderBy( x => x.AssignmentId )
                    .AsEnumerable( );
                assignments = assignments
                    .Where( x => AssignmentId.Contains( x.AssignmentId ) )
                    .Select( x => x.Parent != null ? x.Parent : x );
            }

            if ( Type?.Any( ) == true ) {
                var types = Type.ToList( );
                types.ForEach( x => x.ToLower( ).Replace( " ", "-" ) );
                assignments = assignments.Where( x => types.Contains( x.Type.ToLower( ).Replace( " ", "-" ) ) );
            }

            if ( Priority?.Any( ) == true )
                assignments = assignments.Where( x => Priority.Contains( x.Priority.ToLower( ) ) );

            if ( ProjectId?.Any( ) == true )
                assignments = assignments.Where( x => ProjectId.Contains( x.Project.ProjectId ) );

            if ( Status?.Any( ) == true ) {
                var status = Status.ToList( );
                status.ForEach( x => x.ToLower( ).Replace( " ", "-" ) );
                assignments = assignments.Where( x => status.Contains( x.Status.ToLower( ).Replace( " ", "-" ) ) );
            }

            if ( ExceptStatus?.Any( ) == true ) {
                var status = ExceptStatus.ToList( );
                status.ForEach( x => x.ToLower( ).Replace( " ", "-" ) );
                assignments = assignments.Where( x => !status.Contains( x.Status.ToLower( ).Replace( " ", "-" ) ) );
            }

            var format = "{0,-10%} │ {1,10%} │ {2,25%} │ {3,20%} │ {4,10%} │ {5,15%} │ {6,5%} │ {7,5%}";

            Writer.Title( format, "#  ID", "PROJETO", "NOME", "TIPO", "PRIORIDADE", "SITUAÇÃO", "%", "TEMPO" );

            foreach ( var item in assignments ) {
                PrintTree( item, format );
                Writer.Divider( );
            }
        }

        public void PrintTree( Assignment tree, string format ) {
            List<Assignment> firstStack = new List<Assignment> { tree };

            List<List<Assignment>> childListStack = new List<List<Assignment>> { firstStack };

            while ( childListStack.Count > 0 ) {
                List<Assignment> childStack = childListStack[ ^1 ];

                if ( childStack.Count == 0 )
                    childListStack.RemoveAt( childListStack.Count - 1 );
                else {
                    tree = childStack[ 0 ];
                    childStack.RemoveAt( 0 );

                    string indent = "";
                    for ( int i = 0; i < childListStack.Count - 1; i++ ) {
                        if ( i < childListStack.Count - 2 )
                            indent += childStack.Count >= 0 && childListStack.Count > 0 ? "│  " : "├─ ";
                        else
                            indent += childStack.Count == 0 ? "└─ " : "├─ ";
                    }

                    var time = _timeEntryService.GetTimeByProject( tree.AssignmentId );
                    Writer.WriteLine(
                        format,
                        indent + tree.AssignmentId.ToString( ),
                        tree.Project.ProjectId,
                        tree.Title,
                        tree.Type,
                        tree.Priority,
                        tree.Status,
                        tree.Percent + "%",
                        time );

                    if ( tree.Child.Count( ) > 0 )
                        childListStack.Add( new List<Assignment>( tree.Child ) );
                }
            }
        }
    }
}