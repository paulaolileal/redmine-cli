﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Redmine.Cli.Commands.AssignmentCommands {

    [Command( "new", "n", "add", "a", Description = "Cadastrar uma nova demanda" )]
    public class NewAssignment: CommandWithLogin {

        [Argument( 0, Description = "Id do projeto" )]
        [Required]
        public string ProjectId { get; set; }

        [Argument( 1, Description = "Nome do projeto" )]
        [Required]
        public string Name { get; set; }

        [Argument( 2, Description = "Descrição do projeto" )]
        public string Description { get; set; }

        [Option( CommandOptionType.MultipleValue, Description = "Modulos do SGAP" )]
        public List<string> Modules { get; set; } = new List<string>( );

        [Option( CommandOptionType.SingleValue, Template = "-pr|--priority", Description = "Prioridade da demanda" )]
        public string Priority { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-tx|--text", Description = "Descrição para versionamento" )]
        public string TextForVersion { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-tp|--type", Description = "Tipo de tarefa" )]
        public string Type { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-v|--version", Description = "Versão" )]
        public string Version { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-p|--parent", Description = "Id da tarefa pai" )]
        public long ParentId { get; set; } = 0;

        private readonly AssignmentService _assignmentService;
        private readonly ProjectService _projectService;

        public NewAssignment( ) {
            _projectService = new ProjectService( );
            _assignmentService = new AssignmentService( );
        }

        public override void OnExecute( ) {
            var project = _projectService.Get( x => x.ProjectId == ProjectId );

            if ( project == null )
                throw new Exception( "Projeto não encontrado!" );

            if ( !string.IsNullOrEmpty( Priority ) ) {
                var priority = project.AllowedValues.Priority.FirstOrDefault( x => x.ToLower( ).Replace( " ", "-" ).Contains( Priority?.ToLower( ).Replace( " ", "-" ) ) );
                if ( priority == null )
                    throw new Exception( $"Essa prioridade não é aceita nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.Priority )}" );
                Priority = priority;
            }

            if ( !string.IsNullOrEmpty( Type ) ) {
                var type = project.AllowedValues.AssignmentTypes.FirstOrDefault( x => x.ToLower( ).Replace( " ", "-" ).Contains( Type?.ToLower( ).Replace( " ", "-" ) ) );
                if ( type == null )
                    throw new Exception( $"Esse tipo não é aceito nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.AssignmentTypes )}" );
                Type = type;
            } else
                Type = project.AllowedValues.AssignmentTypes.FirstOrDefault( );

            if ( project.AllowedValues.Modules.Any( ) && ( !Modules.Any( ) || !project.AllowedValues.Modules.Intersect( Modules ).Any( ) ) )
                throw new Exception( $"Esse modulo não é aceito nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.Modules )}" );

            if ( project.AllowedValues.Versions.Any( ) && ( string.IsNullOrEmpty( Version ) || !project.AllowedValues.Versions.Contains( Version ) ) )
                throw new Exception( $"Essa versão não é aceito nesse projeto. Os valores aceitos são: {string.Join( ", ", project.AllowedValues.Versions )}" );

            if ( string.IsNullOrEmpty( TextForVersion ) )
                TextForVersion = "A definir";

            if ( ParentId != 0 ) {
                var parentExist = _assignmentService.Get( x => x.AssignmentId == ParentId ) != null;
                if ( !parentExist )
                    throw new Exception( $"A demanda {ParentId} não existe" );
            }

            Writer.Info( $"Cadastrando demanda: {Name}..." );

            var assignment = new Assignment(
                Name,
                Description,
                Type,
                Priority,
                project.AllowedValues.Status.FirstOrDefault( ),
                Version,
                TextForVersion,
                ParentId,
                Modules,
                project
            );

            _assignmentService.Add( assignment );
            Writer.Success( "Demanda adicionada com sucesso" );
        }
    }
}