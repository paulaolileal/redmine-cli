﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redmine.Cli.Commands.AssignmentCommands {

    [Command( "sync", "s", Description = "Sincronizar as demandas locais com o redmine" )]
    public class SyncAssignment: CommandWithLogin {

        [Option( CommandOptionType.MultipleValue, Description = "Ids dos projetos a serem atualizados" )]
        public List<string> Projects { get; set; } = new List<string>( );

        [Option( CommandOptionType.SingleValue, Template = "-d|--date", Description = "Atualizar a partir de quando, formato dd/MM/yyyy" )]
        public string Data { get; set; }

        private readonly AssignmentService _assignmentService;
        private readonly ProjectService _projectService;

        public SyncAssignment( ) {
            _assignmentService = new AssignmentService( );
            _projectService = new ProjectService( );
        }

        public override void OnExecute( ) {
            DateTime? date = null;
            if ( !string.IsNullOrEmpty( Data ) ) {
                try {
                    date = DateTime.ParseExact( Data, "dd/MM/yyyy", CultureInfo.InvariantCulture );
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: dd/MM/yyyy" );
                }
            }

            var projects = _projectService.GetAll( );

            if ( Projects.Any( ) )
                projects = projects.Where( x => Projects.Contains( x.ProjectId ) );

            if ( date.HasValue ) {
                var project = projects.ToList( );
                project.ForEach( p => p.AssignmentUpdatedAt = date );
                projects = project;
            }

            _assignmentService.Sync( projects.ToList( ) );
        }
    }
}