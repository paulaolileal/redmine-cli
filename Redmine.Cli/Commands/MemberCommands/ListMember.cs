﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;

namespace Redmine.Cli.Commands.MembemCommands {

    [Command( "list", "l", Description = "Listar os membros das equipes" )]
    public class ListMember: CommandBase {
        private readonly MemberService _memberService;

        public ListMember( ) {
            _memberService = new MemberService( );
        }

        public override void OnExecute( ) {
            var members = _memberService.GetAll( );

            var format = "{0,40%} │ {1,20%}";

            Writer.Title( format, "NOME", "CARGO" );

            foreach ( var member in members )
                Writer.WriteLine( format, member.Name, member.Role );

            Writer.Divider( );
        }
    }
}