﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Cli.Commands.BaseCommands;

namespace Redmine.Cli.Commands.ProjectCommands {

    [Command( "project", "proj", "p", Description = "Comandos disponiveis para projetos" )]
    [Subcommand(
        typeof( NewProject ),
        typeof( ListProject ),
        typeof( SyncProject ),
        typeof( DetailProject )
        )]
    public class ProjectBase: CommandBase {
    }
}