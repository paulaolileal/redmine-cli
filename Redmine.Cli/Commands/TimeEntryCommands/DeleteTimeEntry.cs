﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "delete", "del", Description = "Cadastrar um periodo de trabalho fora de hora" )]
    public class DeleteTimeEntry: CommandBase {

        [Option( CommandOptionType.MultipleValue, Template = "-i|--issue", Description = "Enviar tempo gasto nessas demandas" )]
        public IEnumerable<long> Assignments { get; set; } = new List<long>( );

        [Option( CommandOptionType.NoValue, Description = "Confirmar automaticamente a exclusao dos periodos de trabalho" )]
        public bool Confirm { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-e|--end", Description = "Até esse dia: dd/MM/yyyy" )]
        public string DayEnd { get; set; }

        [Option( CommandOptionType.SingleValue, Template = "-s|--start", Description = "A partir desse dia: dd/MM/yyyy" )]
        public string DayStart { get; set; }

        [Option( CommandOptionType.MultipleValue, Template = "-p|--project", Description = "Enviar tempo gasto nesses projetos" )]
        public IEnumerable<string> Projects { get; set; } = new List<string>( );

        [Option( CommandOptionType.MultipleValue, Template = "-t|--time", Description = "Enviar esses tempos gastos" )]
        public IEnumerable<long> TimeEntries { get; set; } = new List<long>( );

        private readonly TimeEntryService _timeEntryService;

        public DeleteTimeEntry( ) {
            _timeEntryService = new TimeEntryService( );
        }

        public override void OnExecute( ) {
            var times = new List<TimeEntry>( );

            if ( Projects.Any( ) )
                times.AddRange( _timeEntryService.GetAll( x => Projects.Contains( x.Assignment.Project.ProjectId ) ) );

            if ( Assignments.Any( ) )
                times.AddRange( _timeEntryService.GetAll( x => Assignments.Contains( x.Assignment.AssignmentId ) ) );

            if ( TimeEntries.Any( ) )
                times.AddRange( _timeEntryService.GetAll( x => TimeEntries.Contains( x.TimeEntryId ) ) );

            if ( !Projects.Any( ) && !Assignments.Any( ) && !TimeEntries.Any( ) )
                times.AddRange( _timeEntryService.GetAll( ) );

            if ( !string.IsNullOrEmpty( DayStart ) ) {
                try {
                    var start = DateTime.ParseExact( DayStart, "dd/MM/yyyy", CultureInfo.InvariantCulture );
                    times = times.Where( x => x.DateStart >= start ).ToList( );
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: dd/MM/yyyy" );
                }
            }

            if ( !string.IsNullOrEmpty( DayEnd ) ) {
                try {
                    var end = DateTime.ParseExact( DayEnd, "dd/MM/yyyy", CultureInfo.InvariantCulture );
                    times = times.Where( x => x.DateEnd <= end ).ToList( );
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: dd/MM/yyyy" );
                }
            }

            times = times
                .Where( x => x.Submited == false )
                .GroupBy( x => new { x.TimeEntryId } )
                .Select( g => g.First( ) )
                .ToList( );

            if ( !Confirm && times.Any( ) ) {
                var format = "{0,5%} │ {1,20%} │ {2,10%} │ {3,35%} │ {4,10%} │ {5,10%} │ {6,10%}";

                Writer.Info( "Os seguintes periodos de trabalhão serão excluidos:" );

                Writer.Title( format, "ID", "PROJETO", "DEMANDA", "TITULO", "DATA", "TEMPO", "ENVIADO" );

                foreach ( var item in times )
                    Writer.WriteLine( format,
                        item.TimeEntryId,
                        item.Assignment.Project.Name,
                        item.Assignment.AssignmentId.ToString( ),
                        item.Assignment.Title,
                        item.DateStart.ToString( "dd/MM/yyyy" ),
                        item.Time.ToString( @"hh\:mm" ),
                        item.Submited ? "SIM" : "NÃO"
                        );

                Confirm = Prompt.GetYesNo( "Confirma a exclusão?", true );
            }

            if ( Confirm && times.Any( ) )
                _timeEntryService.Delete( x => times.Select( x => x.TimeEntryId ).Contains( x.TimeEntryId ) );

            Writer.Success( "Periodo de trabalho removido com sucesso!" );
        }
    }
}