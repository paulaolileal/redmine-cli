﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Models;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Globalization;
using System.Linq;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "start", "s", Description = "Iniciar o periodo de trabalho" )]
    public class StartTimeEntry: CommandBase {

        [Argument( 0, Name = "IssueId", Description = "Id da demanda" )]
        public long AssignmentId { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Comentarios" )]
        public string Comments { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Horario de inicio: HH:mm" )]
        public string Start { get; set; }

        [Option( CommandOptionType.SingleValue, Description = "Tipo de atividade" )]
        public string Type { get; set; }

        private readonly AssignmentService _assignmentService;
        private readonly TimeEntryService _timeEntryService;

        public StartTimeEntry( ) {
            _timeEntryService = new TimeEntryService( );
            _assignmentService = new AssignmentService( );
        }

        public override void OnExecute( ) {
            var assignment = _assignmentService.Get( x => x.AssignmentId == AssignmentId );

            if ( assignment == null ) {
                EnsureLoged( );
                assignment = _assignmentService.GetOnline( AssignmentId );
            }

            if ( assignment == null )
                throw new Exception( "Demanda não existe!" );

            if ( _timeEntryService.GetAll( x => x.DateEnd == null ).Any( ) )
                throw new Exception( "Já existe uma demanda em andamento! Finalize ela para começar outra." );

            var type = assignment.Project.AllowedValues.TimeEntryType.FirstOrDefault( );
            if ( !string.IsNullOrEmpty( Type ) ) {
                type = assignment.Project.AllowedValues.TimeEntryType.FirstOrDefault( x => x.ToLower( ).Contains( Type.ToLower( ) ) );
                if ( string.IsNullOrEmpty( type ) )
                    throw new Exception( $"Esse tipo não é aceito nesse projeto. Os valores aceitos são: {string.Join( ',', assignment.Project.AllowedValues.AssignmentTypes )}" );
            }

            var start = DateTime.Now;
            if ( !string.IsNullOrEmpty( Start ) ) {
                try {
                    var hour = DateTime.ParseExact( Start, "HH:mm", CultureInfo.InvariantCulture );
                    start = DateTime.Today;
                    start = start.Date + hour.TimeOfDay;
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: HH:mm" );
                }
            }

            var timeEntry = new TimeEntry( assignment, type, Comments, start );
            _timeEntryService.Add( timeEntry );

            Writer.Success( "Demanda iniciada com sucesso!" );
        }
    }
}