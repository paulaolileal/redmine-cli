﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Linq;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "status", "stat", Description = "Mostrar o tempo trabalhado na demanda até o momento" )]
    public class StatusTimeEntry: CommandBase {
        private readonly TimeEntryService _timeEntryService;

        public StatusTimeEntry( ) {
            _timeEntryService = new TimeEntryService( );
        }

        public override void OnExecute( ) {
            var timeEntry = _timeEntryService.Get( x => !x.DateEnd.HasValue );
            var todayTimes = _timeEntryService.GetAll( x => x.DateStart.Date == DateTime.Today );
            var totalTime = TimeSpan.FromMilliseconds( todayTimes.Sum( x => x.Time.TotalMilliseconds ) ).ToString( @"hh\:mm" );

            if ( timeEntry == null ) {
                Writer.Warning( $"Não existe demanda em execução agora! - O tempo trabalhado até o momento é: {totalTime}" );
            } else {
                var format = "{0,20%} │ {1,10%} │ {2,40%} │ {3,10%} │ {4,10%} │ {5,10%}";

                Writer.Title( format, "PROJETO", "ID", "NOME", "INICIO AS", "TEMPO", "TOTAL HOJE" );
                Writer.WriteLine( format,
                    timeEntry.Assignment.Project.Name,
                    timeEntry.Assignment.AssignmentId,
                    timeEntry.Assignment.Title,
                    timeEntry.DateStart.ToString( "HH:mm" ),
                    timeEntry.Time.ToString( @"hh\:mm" ),
                    totalTime );
            }
        }
    }
}