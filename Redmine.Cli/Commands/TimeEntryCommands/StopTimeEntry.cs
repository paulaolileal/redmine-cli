﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using Redmine.Cli.Extensions;
using System;
using System.Globalization;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "stop", "st", Description = "Finalizar periodo de trabalho na demanda atual" )]
    public class StopTimeEntry: CommandBase {

        [Option( CommandOptionType.SingleValue, Description = "Horario de finalizar: HH:mm" )]
        public string End { get; set; }

        private readonly TimeEntryService _timeEntryService;

        public StopTimeEntry( ) {
            _timeEntryService = new TimeEntryService( );
        }

        public override void OnExecute( ) {
            var timeEntry = _timeEntryService.Get( x => !x.DateEnd.HasValue );
            if ( timeEntry == null )
                throw new Exception( "Não existe demanda em execução!" );

            var end = DateTime.Now;
            if ( !string.IsNullOrEmpty( End ) ) {
                try {
                    var hour = DateTime.ParseExact( End, "HH:mm", CultureInfo.InvariantCulture );
                    end = timeEntry.DateStart;
                    end = end.Date + hour.TimeOfDay;
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: HH:mm" );
                }
            }

            timeEntry.Finalizar( end );

            _timeEntryService.Update( timeEntry );

            Writer.Success( $"Demanda encerrada com {timeEntry.Time.ToString( @"hh\:mm" )} de trabalho!" );
        }
    }
}