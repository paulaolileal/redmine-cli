﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Application.Services;
using Redmine.Cli.Commands.BaseCommands;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "sync", Description = "Sincronizar os periodos de trabalho locais com o redmine" )]
    public class SyncTimeEntry: CommandWithLogin {

        [Option( CommandOptionType.MultipleValue, Template = "-i|--issue", Description = "Ids das demandas a serem atualizadas" )]
        public List<long> Assignments { get; set; } = new List<long>( );

        [Option( CommandOptionType.MultipleValue, Template = "-p|--project", Description = "Ids dos projetos a serem atualizados" )]
        public List<string> Projects { get; set; } = new List<string>( );

        [Option( CommandOptionType.SingleValue, Template = "-d|--date", Description = "Atualizar a partir de quando, formato dd/MM/yyyy" )]
        public string Data { get; set; }

        private readonly AssignmentService _assignmentService;
        private readonly ProjectService _projectService;
        private readonly TimeEntryService _timeEntryService;

        public SyncTimeEntry( ) {
            _projectService = new ProjectService( );
            _assignmentService = new AssignmentService( );
            _timeEntryService = new TimeEntryService( );
        }

        public override void OnExecute( ) {
            DateTime? date = null;
            if ( !string.IsNullOrEmpty( Data ) ) {
                try {
                    date = DateTime.ParseExact( Data, "dd/MM/yyyy", CultureInfo.InvariantCulture );
                } catch ( Exception ) {
                    throw new Exception( "Erro ao fazer parse da data, o formato deve ser: dd/MM/yyyy" );
                }
            }

            if ( Assignments.Any( ) ) {
                var assignments = _assignmentService.GetAll( );
                assignments = assignments.Where( x => Assignments.Contains( x.AssignmentId ) );

                if ( date.HasValue ) {
                    var assign = assignments.ToList( );
                    assign.ForEach( p => p.Project.TimeEntryUpdatedAt = date );
                    assignments = assign;
                }

                _timeEntryService.Sync( assignments );
            } else {
                var projects = _projectService.GetAll( );
                if ( Projects.Any( ) )
                    projects = projects.Where( x => Projects.Contains( x.ProjectId ) );

                if ( date.HasValue ) {
                    var project = projects.ToList( );
                    project.ForEach( p => p.TimeEntryUpdatedAt = date );
                    projects = project;
                }

                _timeEntryService.Sync( projects );
            }
        }
    }
}