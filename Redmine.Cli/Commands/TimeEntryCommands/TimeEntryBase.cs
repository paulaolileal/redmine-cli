﻿using McMaster.Extensions.CommandLineUtils;
using Redmine.Cli.Commands.BaseCommands;

namespace Redmine.Cli.Commands.TimeEntryCommands {

    [Command( "time", "t", Description = "Comandos disponiveis para periodos de trabalho" )]
    [Subcommand(
        typeof( StartTimeEntry ),
        typeof( StopTimeEntry ),
        typeof( StatusTimeEntry ),
        typeof( SubmitTimeEntry ),
        typeof( NewTimeEntry ),
        typeof( ListTimeEntry ),
        typeof( SyncTimeEntry ),
        typeof( DeleteTimeEntry ),
        typeof( EditTimeEntry )
        )]
    public class TimeEntryBase: CommandBase {
    }
}