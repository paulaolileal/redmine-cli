﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Redmine.Cli.Extensions {

    public static class Writer {

        public static void BreakLine( ) {
            Console.WriteLine( );
        }

        public static void Divider( char symbol = '─' ) {
            Console.WriteLine( Line( symbol ) );
        }

        public static void Error( string text ) {
            WriteLineInColor( text, ConsoleColor.DarkRed );
        }

        public static string FormatToPercent( string format ) {
            var anotherCaracteres = FindExtraCharacteres( format );
            var total = Console.WindowWidth - 1 - string.Join( "", anotherCaracteres ).Length;

            var percents = ExtractFromBody( format, ",", "}" )
                .Where( x => x.Contains( '%' ) )
                .Distinct( )
                .ToList( );

            if ( percents.Select( x => Convert.ToInt32( x.Replace( "%", "" ) ) ).Sum( ) > 100 )
                throw new Exception( "The sum of percents in format is greater than 100%" );

            foreach ( var item in percents ) {
                var isNegative = item.Contains( "-" );
                var proportion = ( isNegative ? -1 : 1 ) * ( total * Convert.ToInt32( item.Replace( "%", "" ).Replace( "-", "" ) ) ) / 100;
                format = format.Replace( "," + item, "," + proportion.ToString( ) );
            }
            return format;
        }

        public static void Info( string text ) {
            WriteLineInColor( text, ConsoleColor.Blue );
        }

        public static string Line( char symbol = '─' ) {
            var width = Console.WindowWidth - 1;
            return string.Empty.PadRight( width, symbol );
        }

        public static void Success( string text ) {
            WriteLineInColor( text, ConsoleColor.Green );
        }

        public static void Title( string format, params string[ ] text ) {
            Writer.Divider( );
            WriteLineInColor( format, ConsoleColor.DarkYellow, text );
            Writer.Divider( );
        }

        public static void Warning( string text ) {
            WriteLineInColor( text, ConsoleColor.Yellow );
        }

        public static void WriteInColor( string text, ConsoleColor color ) {
            Console.ForegroundColor = color;
            Console.Write( text );
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WriteLine( string format, params object[ ] text ) {
            var formated = format;
            if ( format.Contains( '%' ) )
                formated = FormatToPercent( format );
            var values = ExtractFromBody( formated, ",", "}" );
            for ( int i = 0; i < text.Length; i++ ) {
                if ( text[ i ] != null )
                    text[ i ] = text[ i ].ToString( ).TextOverflow( Convert.ToInt32( values[ i ].Replace( "-", "" ) ) );
            }
            Console.WriteLine( formated, text );
        }

        public static void WriteLineInColor( string text, ConsoleColor color ) {
            Console.ForegroundColor = color;
            Console.WriteLine( text );
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WriteLineInColor( string format, ConsoleColor color, params string[ ] text ) {
            Console.ForegroundColor = color;
            Writer.WriteLine( FormatToPercent( format ), text );
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static char[ ] FindExtraCharacteres( string format ) {
            var anotherCaracteres = ExtractFromBody( format, "{", "}" );

            foreach ( var item in anotherCaracteres )
                format = format.Replace( new string( $"{{{item}}}" ), "" );

            return format.ToArray( );
        }

        private static List<string> ExtractFromBody( string body, string start, string end ) {
            List<string> matched = new List<string>( );

            int indexStart = 0;
            int indexEnd = 0;

            bool exit = false;
            while ( !exit ) {
                indexStart = body.IndexOf( start );

                if ( indexStart != -1 ) {
                    indexEnd = indexStart + body.Substring( indexStart ).IndexOf( end );

                    if ( indexStart > indexEnd )
                        break;

                    matched.Add( body.Substring( indexStart + start.Length, indexEnd - indexStart - start.Length ) );

                    body = body.Substring( indexEnd + end.Length );
                } else {
                    exit = true;
                }
            }

            return matched;
        }
    }
}