﻿using ShellProgressBar;
using System;

namespace Redmine.Commons {

    public class Settings {
        public static string AppData { get; set; }
        public static string ConnectionString { get; set; }
        public static string RedmineUrl { get; set; }
        public static bool Visual { get; set; }

        public static ProgressBarOptions Options = new ProgressBarOptions {
            BackgroundColor = ConsoleColor.DarkGray,
            BackgroundCharacter = '─',
            ProgressCharacter = '─',
            ProgressBarOnBottom = true,
            DisplayTimeInRealTime = false
        };

        public static ProgressBarOptions ChildOptions = new ProgressBarOptions {
            ForegroundColor = ConsoleColor.DarkYellow,
            BackgroundColor = ConsoleColor.DarkGray,
            BackgroundCharacter = '─',
            ProgressCharacter = '─',
            ProgressBarOnBottom = true,
            DisplayTimeInRealTime = false,
            CollapseWhenFinished = true
        };
    }
}