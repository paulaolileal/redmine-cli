using Redmine.Application.Models;
using Redmine.Application.Services;
using Xunit;

namespace Redmine.Test {

    public class AssigmentTest: TestBase {

        [Fact]
        public void Assignment_must_add( ) {
            var service = new AssignmentService( );
            var pService = new ProjectService( );

            var assignment = new Assignment {
                Description = "Teste 02",
                Title = "Teste 02",
                // Type = AssignmentType.Demanda,
                Project = pService.Get( x => x.ProjectId == "projeto-de-teste" )
            };

            service.Add( assignment );
        }

        [Fact]
        public void Assignment_must_be_initialized( ) {
            var service = new AssignmentService( );
            // service.Sync( "projeto-de-teste" );
        }
    }
}