using Redmine.Application;
using Redmine.Application.Services;
using System.Threading.Tasks;
using Xunit;

namespace Redmine.Test {

    public class MemberTest: TestBase {

        [Fact]
        public async Task Members_must_be_initializedAsync( ) {
            var service = new MemberService( );
            service.Sync( null );

            Browser.Close( );
        }
    }
}