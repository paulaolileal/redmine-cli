﻿using Microsoft.Extensions.Configuration;
using Redmine.Application;
using Redmine.Application.Services;
using Redmine.Commons;
using System.IO;

namespace Redmine.Test {

    public abstract class TestBase {
        private readonly IConfiguration _configuration;

        public TestBase( ) {
            _configuration = BuildConfiguration( );
            Settings.ConnectionString = _configuration.GetConnectionString( "DefaultConnection" );
            Settings.RedmineUrl = _configuration.GetSection( "RedmineUrl" ).Value;

            EnsureLogged( );

            //var database = Path.Combine( Directory.GetCurrentDirectory( ), "redmine.litedb");
            //if ( File.Exists( database ) )
            //    File.Delete( database );

            //var databaselog = Path.Combine( Directory.GetCurrentDirectory( ), "redmine-log.litedb" );
            //if ( File.Exists( databaselog ) )
            //    File.Delete( databaselog );
        }

        ~TestBase( ) {
            Browser.Close( );
        }

        private static IConfiguration BuildConfiguration( ) {
            return new ConfigurationBuilder( )
                                .SetBasePath( Directory.GetCurrentDirectory( ) )
                                .AddJsonFile( "appsettings.json", optional: true, reloadOnChange: true )
                                .Build( );
        }

        private void EnsureLogged( ) {
            var userService = new UserService( );
            userService.Login( );
        }
    }
}