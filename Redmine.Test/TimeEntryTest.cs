using Redmine.Application.Models;
using Redmine.Application.Services;
using System;
using Xunit;

namespace Redmine.Test {

    public class TimeEntryTest: TestBase {

        [Fact]
        public void Assignment_must_add( ) {
            var service = new TimeEntryService( );
            var aService = new AssignmentService( );

            var assignment = new TimeEntry {
                DateStart = DateTime.Now.AddHours( -1 ).AddMinutes( -15 ),
                DateEnd = DateTime.Now,
                Comments = "Teste de te",
                Assignment = aService.Get( x => x.AssignmentId == 286719 )
            };

            //service.Submit( assignment );
        }
    }
}